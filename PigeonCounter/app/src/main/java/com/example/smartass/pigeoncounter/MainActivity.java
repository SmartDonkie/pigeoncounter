package com.example.smartass.pigeoncounter;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.icu.util.Calendar;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    public static int DEFAULT_TIME_HOUR_FROM = 6;
    public static int DEFAULT_TIME_MINUTE_FROM = 0;
    public static int DEFAULT_TIME_HOUR_TO = 7;
    public static int DEFAULT_TIME_MINUTE_TO = 0;
    //top
    TextView tv_date;
    EditText et_time_from, et_time_to,et_area;
    private int mYear, mMonth, mDay;
    private RecyclerView rv_mid;

    //data parameters
    private int setTimeFromHour, setTimeFromMin;
    private int setTimeToHour, setTimeToMin;
    private int setDateYear, setDateMonth, setDateDay;
    private int nowEditRow;
    private EditText nowEditText;
    private RvAdpeter rvAdpeter;
    private ArrayList<Integer> dataQ;

    //numpad
    private ImageButton btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0, btnOk, btnBack;
    private ImageButton btnPlus1;
    private TextView tv_nowEditRow;
    private LinearLayout lln_main_botton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        Intent intent = new Intent(MainActivity.this,DataActivity.class);
//        startActivity(intent);
        //top find views
        tv_date = findViewById(R.id.tv_date);
        et_time_from = findViewById(R.id.et_time_from);
        et_time_to = findViewById(R.id.et_time_to);
        et_area = findViewById(R.id.et_main_area);
        et_time_from.setFocusable(false);
        et_time_to.setFocusable(false);
        //recycler find views
        dataQ = new ArrayList<>();
        rv_mid = findViewById(R.id.rv_main_data);
        rv_mid.setLayoutManager(new LinearLayoutManager(this));
        timeDateInit();

        View.OnClickListener timeOnClick = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.et_time_from:
                        timeClick(v, et_time_from, DEFAULT_TIME_HOUR_FROM, DEFAULT_TIME_MINUTE_FROM, true);
                        break;
                    case R.id.et_time_to:
                        timeClick(v, et_time_to, DEFAULT_TIME_HOUR_TO, DEFAULT_TIME_MINUTE_TO, false);
                        break;
                }
            }
        };
        et_time_from.setOnClickListener(timeOnClick);
        et_time_to.setOnClickListener(timeOnClick);
        rvAdpeter = new RvAdpeter(dataQ);
        rv_mid.setAdapter(rvAdpeter);

        lln_main_botton = findViewById(R.id.lln_main_botton);
        ArrayList<ImageButton> btnArr = new ArrayList<>();
        btn1 = findViewById(R.id.ibtn_1);
        btn2 = findViewById(R.id.ibtn_2);
        btn3 = findViewById(R.id.ibtn_3);
        btn4 = findViewById(R.id.ibtn_4);
        btn5 = findViewById(R.id.ibtn_5);
        btn6 = findViewById(R.id.ibtn_6);
        btn7 = findViewById(R.id.ibtn_7);
        btn8 = findViewById(R.id.ibtn_8);
        btn9 = findViewById(R.id.ibtn_9);
        btn0 = findViewById(R.id.ibtn_0);
        btnBack = findViewById(R.id.ibtn_back);
        btnPlus1 = findViewById(R.id.ibtn_plus1);
        btnArr.add(btn1);
        btnArr.add(btn2);
        btnArr.add(btn3);
        btnArr.add(btn4);
        btnArr.add(btn5);
        btnArr.add(btn6);
        btnArr.add(btn7);
        btnArr.add(btn8);
        btnArr.add(btn9);
        btnArr.add(btn0);
        btnArr.add(btnBack);
        btnArr.add(btnPlus1);
        View.OnClickListener btnOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(nowEditText != null){
                    String btnText = "";
                    Editable editable = nowEditText.getEditableText();
                    switch (v.getId()) {
                        case R.id.ibtn_1:
                            btnText = "1";
                            break;
                        case R.id.ibtn_2:
                            btnText = "2";
                            break;
                        case R.id.ibtn_3:
                            btnText = "3";
                            break;
                        case R.id.ibtn_4:
                            btnText = "4";
                            break;
                        case R.id.ibtn_5:
                            btnText = "5";
                            break;
                        case R.id.ibtn_6:
                            btnText = "6";
                            break;
                        case R.id.ibtn_7:
                            btnText = "7";
                            break;
                        case R.id.ibtn_8:
                            btnText = "8";
                            break;
                        case R.id.ibtn_9:
                            btnText = "9";
                            break;
                        case R.id.ibtn_0:
                            btnText = "0";
                            break;
                        case R.id.ibtn_plus1:
                            int nowQ;
                            try{
                                nowQ = Integer.parseInt(editable.toString());
                            }catch (NumberFormatException e){
                                nowQ = 0;
                            }
                            nowQ++;
                            nowEditText.setText(nowQ + "");
                            return;
                        case R.id.ibtn_back:
                            int length = editable.length();
                            if(length < 1){
                                return;
                            }
                            editable.delete(length-1,length);
                            return;

                    }
                    if(editable.toString().equals("0")){
                        nowEditText.setText(btnText);
                        return;
                    }else {
                        nowEditText.append(btnText);
                        return;
                    }
                }
            }
        };
        for (int i = 0; i < btnArr.size(); i++){
            btnArr.get(i).setOnClickListener(btnOnClickListener);
        }

    }

    public class RvAdpeter extends RecyclerView.Adapter<RvAdpeter.ItemHolder>{
        int totalRow;
        ArrayList<Integer> dataQ;
        public RvAdpeter(ArrayList<Integer> dataQ){
            this.dataQ = dataQ;
            countTime();
        }
        public void countTime(){
            int totalMinRange = (setTimeToHour - setTimeFromHour) * 60 + setTimeToMin - setTimeFromMin;
            if (totalMinRange > 0){
                totalRow = totalMinRange / 5;
            }
            while (dataQ.size() > totalRow){
                dataQ.remove((dataQ.size()-1));
            }
            while (dataQ.size() < totalRow) {
                dataQ.add(0);
            };
        }
        @NonNull
        @Override
        public RvAdpeter.ItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            if(viewType == 1){
                View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_main_save,parent,false);
                return new LastButtonHolder(view);
            }
            View view = LayoutInflater.from(MainActivity.this).inflate(R.layout.item_main_time_label,parent,false);
            return new SingleDataHolder(view);
        }

        @Override
        public int getItemViewType(int position) {
            if(position == totalRow){
                return 1;
            }
            return 0;
        }

        @Override
        public void onBindViewHolder(@NonNull RvAdpeter.ItemHolder holder, int position) {
            holder.onBind(position);
        }

        @Override
        public int getItemCount() {
            return totalRow;
        }

        //Father abstract Holder
        protected abstract class ItemHolder extends RecyclerView.ViewHolder{

            public ItemHolder(@NonNull View itemView) {
                super(itemView);
            }
            public abstract void onBind(int position);
        }
        //Son SingleDataHolder
        protected class SingleDataHolder extends ItemHolder{
            EditText et_q, et_note;
            TextView tv_order, tv_time;
            public SingleDataHolder(@NonNull View itemView) {
                super(itemView);
                et_note = itemView.findViewById(R.id.et_item_main_note);
                et_q = itemView.findViewById(R.id.et_item_main_q);
                tv_order = itemView.findViewById(R.id.tv_item_time_order);
                tv_time = itemView.findViewById(R.id.tv_item_time);
                et_q.setShowSoftInputOnFocus(false);
                et_q.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(hasFocus){
                            nowEditRow = getLayoutPosition();
                            nowEditText = et_q;
                            InputMethodManager imm = (InputMethodManager) MainActivity.this.getSystemService(Activity.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(et_q.getWindowToken(), 0);
                        }else {
                            try{
                                dataQ.set(getLayoutPosition(),Integer.parseInt(et_q.getText().toString()));
                            }catch (NumberFormatException e){

                            }
                        }
                    }
                });
                et_note.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(hasFocus){
                            hideBotton();
                        }else {
                            showBotton();
                        }
                    }
                });

            }

            @Override
            public void onBind(int position){
                int hour = setTimeFromHour + (setTimeFromMin + ((position + 1) * 5)) / 60;
                int minute = (setTimeFromMin + ((position + 1) * 5)) % 60;
                tv_time.setText(hour + ":" + minute);
                tv_order.setText((position + 1) + "");
                int q = dataQ.get(position);
                if(q != 0){
                    et_q.setText(dataQ.get(position)+"");
                }
            }
        }
        //Son Last ButtonHolder
        public class LastButtonHolder extends ItemHolder{

            public LastButtonHolder(@NonNull View itemView) {
                super(itemView);
            }

            @Override
            public void onBind(int position) {

            }
        }

        public void notifyTimeChange(){
            countTime();
            this.notifyDataSetChanged();
        }
    }

    public void timeClick(View view, final EditText editText, int hour, int minute, final boolean from) {
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                editText.setText(hourOfDay + ":" + minute);
                if (from) {
                    setTimeFromHour = hourOfDay;
                    setTimeFromMin = minute;
                    rvAdpeter.notifyTimeChange();
                    Log.i("Time", "From " + setTimeFromHour + ":" + setTimeFromMin);


                } else {
                    setTimeToHour = hourOfDay;
                    setTimeToMin = minute;
                    rvAdpeter.notifyTimeChange();
                    Log.i("Time", "To " + setTimeToHour + ":" + setTimeToMin);
                }

            }
        }, hour, minute, false);
        timePickerDialog.show();
    }

    public void dateClick(View view) {
        DatePickerDialog datePickerDialog = new DatePickerDialog(this);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                setDateYear = year;
                setDateMonth = month;
                setDateDay = dayOfMonth;
                tv_date.setText(year + "/" + (month + 1) + "/" + dayOfMonth);
            }
        });
        datePickerDialog.show();
    }

    private void timeDateInit() {
        //Date
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        tv_date.setHint(mYear + "/" + (mMonth + 1) + "/" + mDay);  // show month+1
        setDateYear = mYear;
        setDateMonth = mMonth;
        setDateDay = mDay;
        //Time
        setTimeFromHour = DEFAULT_TIME_HOUR_FROM;
        setTimeFromMin = DEFAULT_TIME_MINUTE_FROM;
        Log.i("Time", "From " + setTimeFromHour + ":" + setTimeFromMin);
        setTimeToHour = DEFAULT_TIME_HOUR_TO;
        setTimeToMin = DEFAULT_TIME_MINUTE_TO;
        Log.i("Time", "To " + setTimeToHour + ":" + setTimeToMin);
        et_time_from.setText(setTimeFromHour + ":" + setTimeFromMin);
        et_time_to.setText(setTimeToHour + ":" + setTimeToMin);

    }

    public void hideBotton(){
        lln_main_botton.setVisibility(View.GONE);
    }
    public void showBotton(){
        lln_main_botton.setVisibility(View.VISIBLE);
    }

}

package com.example.smartass.pigeoncounter;

import android.content.Context;

public class Database {
    //想要做成一個singleton
    private static Database database;
    private Database(Context context){

    }

    public static Database getInstance(Context context){
        if(database == null){
            database = new Database(context);
        }
        return database;
    }
}

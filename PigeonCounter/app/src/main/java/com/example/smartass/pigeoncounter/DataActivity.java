package com.example.smartass.pigeoncounter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class DataActivity extends AppCompatActivity {
    RecyclerView rv_historyDataHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data);
        setRv_historyDataHeader();
        ArrayList<DailyData> dailyDataArrayList;
        dailyDataArrayList = new ArrayList<>();
        dailyDataArrayList.add(new DailyData("2018/11/12","A區"));
        dailyDataArrayList.add(new DailyData("2018/11/13","B區"));
        dailyDataArrayList.add(new DailyData("2018/11/14","C區"));
        dailyDataArrayList.add(new DailyData("2018/11/15","C區"));
        dailyDataArrayList.add(new DailyData("2018/11/16","B區"));
        dailyDataArrayList.add(new DailyData("2018/11/17","A區"));
        dailyDataArrayList.add(new DailyData("2018/11/18","C區"));
        dailyDataArrayList.add(new DailyData("2018/11/19","A區"));
        DataAdapter dataAdapter = new DataAdapter(this, dailyDataArrayList);
        rv_historyDataHeader.setAdapter(dataAdapter);
    }

    private void setRv_historyDataHeader(){
        rv_historyDataHeader = findViewById(R.id.rv_historyDataHeader);
        RecyclerView.LayoutManager llm_historyData= new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        rv_historyDataHeader.setLayoutManager(llm_historyData);
    }

    private class DataAdapter extends RecyclerView.Adapter<DataAdapter.CustomHolder>{
        Context context;
        ArrayList<DailyData> dailyDataArrayList;
        public DataAdapter(Context context, ArrayList<DailyData> dailyDataArrayList){
            this.context = context;
//            this.dailyDataArrayList = new ArrayList<DailyData>(dailyDataArrayList); // copy constructor!!
            this.dailyDataArrayList = dailyDataArrayList;
        }

        @NonNull
        @Override
        public DataAdapter.CustomHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_history_data_header,parent,false);
            final DataHolder holder = new DataHolder(view);
            holder.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    dailyDataArrayList.remove(holder.getLayoutPosition());
                    DataAdapter.this.notifyItemRemoved(holder.getLayoutPosition());    //refresh the dataAdapter
                    return true;
                }
            });
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull DataAdapter.CustomHolder holder, int position) {  //每次捲動都會執行
            holder.onBind(dailyDataArrayList.get(position));
        }

        @Override
        public int getItemViewType(int position) {  //return its view type
            return position%2;
        }

        @Override
        public int getItemCount() {
            return dailyDataArrayList == null ? 0 : dailyDataArrayList.size();
        }

        protected class DataHolder extends CustomHolder<DailyData>{   //這個viewholder會被重複使用，所以可能100個資料共用5個，此方法只會被執行5次之類的
            TextView tv_item_date;
            TextView tv_item_area;
            public DataHolder(@NonNull View itemView){
                super(itemView);
                tv_item_area = itemView.findViewById(R.id.tv_item_area);
                tv_item_date = itemView.findViewById(R.id.tv_item_date);

            }

            @Override
            public void onBind(DailyData dailyData) {
                dailyData.getArea();
                dailyData.getDate();
            }

            @Override
            public void setOnClickListener(View.OnClickListener onClickListener) {
                itemView.setOnClickListener(onClickListener);
            }

            @Override
            public void setOnLongClickListener(View.OnLongClickListener onLongClickListener) {
                itemView.setOnLongClickListener(onLongClickListener);
            }
        }

        public abstract class CustomHolder <T extends DailyData>extends RecyclerView.ViewHolder{
            public CustomHolder(@NonNull View itemView) {
                super(itemView);
            }

            public abstract void onBind(T data);

            public abstract void setOnClickListener(View.OnClickListener onClickListener);
            public abstract void setOnLongClickListener(View.OnLongClickListener onLongClickListener);
        }
    }
}

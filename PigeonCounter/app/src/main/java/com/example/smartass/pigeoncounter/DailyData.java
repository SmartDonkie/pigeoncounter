package com.example.smartass.pigeoncounter;

import java.util.ArrayList;

public class DailyData {
    private String date;
    private String area;
    private int dateYear, dateMonth, dateDay;
    private ArrayList<SingleTimeData> singleTimeDataArrayList;
    public DailyData(String date, String area){
        this.area = area;
        this.date = date;
    }

    public String getArea() {
        return area;
    }

    public String getDate() {
        return date;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static class SingleTimeData{
        private String time;
        private int quanity;
        private String note;
    }
}
